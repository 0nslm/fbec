# fbec

**FB Events Calendar:** micro webapp fetching data about events from FB pages and/or groups and/or users, which the app's user selects beforehand, and displaying it in a clear and well-arranged (maybe calendar-like...) manner.

**Motivation (purely personal):** I'd like to continue utilizing myslelf Facebook's *"Events"* agenda without that well-known forced necessity to log into the Facebook, being forced to do clicks there, seeing ads and various "walls", etc. I just want to have a good overview of actual events, of which I'm interested in, but without all the Facebook's bullshits.

**Technology used:**

* Nuxt.js - fronted framework built on top of the Vue.js with its own state, etc.
* No database, no backend. (Looking back, that wasn't a smartest decision at all:-D)
* Express.js - used for purpose of mock (fake) data "backend".

## `2022-05-19` STATUS

The project has been in **coma-like** state for ca. almost 3 yrs due to a lack of time, caused by much more important life's events (complete career change, etc.). 

**But I have the main idea still in my head and I don't want to get rid of it.** I'd like re-think, re-design and completely re-work this small hobbyist project of mine in a near future with my current knowledge and skills. I.e. build it entirely on different technologies, add its own database and API, etc.


---


#### Build Setup

``` bash
# install dependencies
yarn install

# serve with hot reload at localhost:3000
yarn dev

# build for production and launch server
yarn build
yarn start

# generate static project
yarn generate
```

For detailed explanation on how things work, checkout [Nuxt.js docs](https://nuxtjs.org).

#### Setup History

```bash
git clone git@gitlab.com:0nslm/fbec.git
cd fbec
npx create-nuxt-app .
npm install --save axios
npm install --save-dev json-server
npm install --save-dev node-sass sass-loader
npm install --save-dev pug pug-plain-loader
yarn install
yarn add nuxt-buefy
```

#### Mock API Server

``` bash
# run from static JSON file with a few of manually acquired sample data and A LOT of mock random data
json-server --watch mock_api/db_final.json --port 3001
```

``` bash
# how to start over with freshly generated mock data
rm -f mock_api/db.json
rm -f mock_api/db_final.json
node mock_api/createDBjsonFromManualSamples.js
node mock_api/mock_api_app.js
```
