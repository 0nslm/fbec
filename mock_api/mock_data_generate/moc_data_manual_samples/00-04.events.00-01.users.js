module.exports = () => {
  return {
    events: [
      {
        id: 0,
        related_to_userId: [0],
        created_by_pageId: [4],
        description: `Tradiční událost v rámci festivalu Česko-Polské Pojivo.\nhttps://www.facebook.com/events/454050238712358/`,
        end_time: '2019-07-28T05:00:00+0200',
        name: 'Tralala Stan na Travné',
        place: {
          name: 'Travná, 790 70 Javorník, Česká Republika'
        },
        start_time: '2019-07-27T17:00:00+0200',
        fid_event: '326521764903496',
        event_picture_url:
          'https://scontent.xx.fbcdn.net/v/t1.0-9/s720x720/64705235_2167496086681611_6957017343507562496_o.jpg?_nc_cat=110&_nc_oc=AQluWqz3QMPOVz1oSYX8XASqKzJLv0JOjr0jMNTqKlkE1DBRpIxLLeLeRByCwSkak94&_nc_ht=scontent.xx&oh=9311dd23ebab19d0189e879cecbe4758&oe=5DA0B0DD',
        // event_url: `https://www.facebook.com/events/${fid_event}`
        rsvp_status: 'unsure'
      },
      {
        id: 1,
        related_to_userId: [0, 1],
        created_by_pageId: [3, 4],
        description: `☮ ☮ ☮ S U N R I C E ‧‧‧ 2 0 1 9 ☮ ☮ ☮\n\nPsychedelic music art free festival\n\nAhoj přátelé a kamarádi,\nzveme vás na již čtvpokračování festivalu psychedelické hudby a umění, jež se uskuteční na louce s rybníčkem na severu Moravy.\n\nTi, kteří s námi prožili předchročníky, ví, že akce bude probíhat dle konceptu freeparty, která bude bez vstupu a “bez bariér a kde všichni účastníci přispívají na průakce konzumací u nás, svým uvědoměním, empatií a zejména svou pozitivně naladěnou přítomností.\n\nI letos se pokusíme posunout festival na vyšlevel, podělíme se s vámi o intenzivní audiovizuální zážitek, který potrvá 4 dny.To zajistí plejáda live projektů, DJs, VJs a všemožných dalšumělců i vystoupení, přesahující hranice žánrů - to vše v unikátní atmosféře a prostředí, které již nejednou nechalo účastníky odjížd�pocitem velmi vydařeného zážitku.\n\nRespektujeme všechny účastníky, kteří berou ohled na své okolí a přírodu.\n`,
        end_time: '2019-07-07T12:00:00+0200',
        name: 'SunRice 2019',
        place: {
          name: 'Ryžoviště, Moravskolezský Kraj, Czech Republic',
          location: {
            city: 'Ryzoviste',
            country: 'Czech Republic',
            latitude: 49.8667,
            longitude: 17.35
          },
          fid_place: '107039395993044'
        },
        start_time: '2019-07-04T20:00:00+0200',
        fid_event: '2218676108421633',
        event_picture_url:
          'https://scontent.xx.fbcdn.net/v/t1.0-9/s720x720/58033199_2290637094328303_650629563285504000_o.jpg?_nc_cat=104&_nc_oc=AQljLS6Jm07pnHf_uA-fVp3fd9UBEtbemEiGFLuepXT9hczzXD_B_7nE9jzluHnjMzQ&_nc_ht=scontent.xx&oh=cb5e5ac7afd428caf9416ad091abda18&oe=5DA14CF0',
        // event_url: `https://www.facebook.com/events/${fid_event}`
        rsvp_status: 'unsure'
      },
      {
        id: 2,
        related_to_userId: [0],
        created_by_pageId: [5, 9],
        description: `/CZ/\nSérie audiovizuálních akcí Banket se vrací na Hlubinu, kde začala. Princip proměnlivosti se ani tentokrát nemění, takže se opět můžete těšit na něco jiného a nového. Variabilita spojená s každou další akcí sleduje původní myšlenku pořadatelů sdružených do kolektivu Audio_Video_Bass, kterou bylo reagovat na nesmyslnou rozdrobenost Ostravy využitím jejích proluk a prázdných prostor pro pořádání hudebních akcí. Střídání party venues se odráží také v hudbě, proto se hudební selekce Banketu pohybuje na pomezí jednotlivých žánrů taneční hudby a svou různorodostí připomíná třeba perský koberec.\n\nPerský koberec připomíná i žánrové zakotvení dalšího Banketu. Po minulém house večírku se vracíme zpět na pole bass music. Tato multižánrová kategorie je výsledkem křížení různých hudebních stylů se zvukovými postupy, kterými zhruba před deseti lety obohatil taneční hudbu dubstep. Podobně jako perský koberec, obsahující mnoho různých vzorů sjednocených charakteristickým stylem (charakteristickou estetikou) perských tkalců, obsahuje i bass music množství různých prvků vypůjčených z jednotlivých žánrů taneční hudby svázaných dohromady hutnými basovými linkami.\n\nJednoznačně nejednoznačná podstata bass music nesedí každému, ale pražská djka STEP BY STEP ji ve svých setech vystihuje velmi dobře, proto jsme ji v rámci celého večera vyhradili headlinerský slot. Na suportu se klasicky představí AVB residenti DLKT, Flu, Moskalus a Toss.\n\nVizuální koncept celé akce, který má stejnou váhu jako její hudební stránka, budou mít na starosti VJs ABUTTUBA a Epix. Jejich AVB dílnička už zase několik posledních týdnů funguje v režimu 24/7, takže kromě samozřejmého vjingu vydá určitě i mnohem víc v rámci dynamického osvětlení prostru. Dřevěná terasa od CO KAFE Dolní Vítkovice, kam se Banket z prostor před kavárnou Maryčka přesouvá, nabídne po vzhledové stránce zase něco nového a zvukově se posuneme o stupeň výš bo dřevo.`,
        end_time: '2019-06-30T04:00:00+0200',
        name: 'Banket(13)',
        place: {
          name: 'CO KAFE Dolní Vítkovice',
          location: {
            city: 'Ostrava',
            country: 'Czech Republic',
            latitude: 49.821559511578,
            longitude: 18.277891874313,
            street: 'Vítkovická 3369',
            zip: '702 00'
          },
          fid_place: '195820614349673'
        },
        start_time: '2019-06-29T21:00:00+0200',
        fid_event: '480851129120901',
        event_picture_url:
          'https://scontent.xx.fbcdn.net/v/t1.0-9/s720x720/62265080_1168362226678027_93521611495309312_o.jpg?_nc_cat=104&_nc_oc=AQnkhcNAgp83-9dBrQoxbX9jieKrLFQXO2RK20_XdzjTKBpsY2xwuCHQYtMv97I69gY&_nc_ht=scontent.xx&oh=f080a6df11524ff1c4b052805a1a3650&oe=5DA6B692',
        // event_url: `https://www.facebook.com/events/${fid_event}`
        rsvp_status: 'attending'
      },
      {
        id: 3,
        related_to_userId: [1],
        created_by_pageId: [16],
        description: `----- Jak surfovat a neutopit se----\n\nCo všechno se o vás dá na internetu zjistit?\nKdo všechno pracuje s vašimi daty a proč?\nMáte možnost ovlivnit, co se s vaším soukromím děje?\nPoužíváte vy a vaše děti sociální sítě bezpečně?\n\n..kdo nedělá nic špatného, nemá co skrývat..” ANO/NE?\n\nNa internetu sdílíme vědomě i nevědomě obrovské množství informací.S tím je spojeno plno skvělých možností a pohodlí, ale také velká míra rizika.Pozvali jsme uznávané experty a ikony československého digitálního světa, abychom společně nahlédli na téma bezpečnosti v dnešním online světě.Naučíte se zacházet se sdílením informací o sobě, zabezpečovat své profily a účty a předcházet digitálním útokům.\nHACKEŘÍ: Kdo jsou, co dělají a jak se jim bránit.\nSOCIÁLNÍ SÍTĚ: Nastavení soukromí a zabezpečení účtu.\nPRAKTICKÉ NÁSTROJE: Efektivní nástroje pro bezpečnost na síti.\nPANELOVÁ DISKUZE: Je soukromí jedince v ohrožení?\nVLÁDNÍ SLEDOVÁNÍ: Aktuální kauzy, odposlechy a masové sledování občanů.\n\nVstup za cenu 290 Kč pouze do 14.6.\nWeb akce je www.digihuman.cz.\nVstupenky na GoOut: http://bit.ly/mojesoukromivbezpeci\n\n------ Co je na programu ? ------\n\nMimo jiné bezpečnost a soukromí v rukou vlád a technologických gigantů\n\nI přesto, že různé internetové hrozby způsobené viry, malwary nebo cílenými útoky jsou dobře známé, většina lidí si není vědomá přetrvávajících rizik, které prosazuje vládní legislativa.Cílem prezentace je popsat tyto hrozby, jako je masivní sledování občanů, globální cenzura a další.Jsme svědky zajímavého paradoxu, kdy se vlády, zodpovědné za nové právní předpisy o ochraně soukromí (GDPR, článek 11 / 13 zákona EU o autorském právě), zároveň stávají nejnebezpečnější hrozbou pro soukromí občanů.\n\n--------- Kdo vystoupí?---------\n\nPavol Lupták\nEtický hacker, člen skupiny Ztohoven, spoluzakladatel Paralelní Polis a expert na kryptoměny.CEO společnosti Nethemba, která se zaměřuje na bezpečnostní audity a penetrační testy velkých organizací (např.bank).\n\nMartin Zeman\nVýzkumník a datový analytik.Jako absolvent oboru částicové a jaderné fyziky se věnoval práci na experimentech v laboratoři CERN.V současné době pracuje jako softwarový inženýr v Protonmailu, kde se snaží vytvářet bezpečnější digitální svět.\n\nHedvika Pajerová\nSocial media manager na volné noze.Přes 7 let zejména IT společnostem se správou, nastavením strategie, reklamami a komunitním managementem.Mentoruje top manažery v oblasti osobního brandingu a ochrany soukromí na sociálních sítích.\n\nJiří Pudich\nBezpečnostní expert, bývalý dlouholetý policejní vyšetřovatel a pracovník policejní inspekce.V současnosti působí v komerční sféře, kde s týmem společnosti Premium Systems přes 8 let řeší bezpečnostní incidenty ve firmách a organizacích.\'\n\nVstup za cenu 290 Kč pouze do 14.6.\nWeb akce je www.digihuman.cz.\nVstupenky na GoOut: http://bit.ly/mojesoukromivbezpeci\n`,
        end_time: '2019-06-22T19:00:00+0200',
        name: 'Ne/bezpečí v síti',
        place: {
          name: 'Impact Hub Ostrava',
          location: {
            city: 'Ostrava',
            country: 'Czech Republic',
            latitude: 49.8393,
            longitude: 18.29233,
            street: 'Sokolská třída 1263/24',
            zip: '702 00'
          },
          fid_place: '697475150292326'
        },
        start_time: '2019-06-22T13:00:00+0200',
        fid_event: '442315576592393',
        event_picture_url:
          'https://scontent.xx.fbcdn.net/v/t1.0-9/s720x720/61659411_2467296533310170_2761639361165393920_o.jpg?_nc_cat=102&_nc_oc=AQmJs6UWsJ6XwjrW4RBBcUNw-0BL5RDROlpSl0kqv3ElZwLBipQIqpe3X5NncBx0YrE&_nc_ht=scontent.xx&oh=11fc75d9be3e9d55ba1c77a65982b1d5&oe=5DAA6408',
        // event_url: `https://www.facebook.com/events/${fid_event}`
        rsvp_status: 'attending'
      }
    ]
  }
}
