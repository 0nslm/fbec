module.exports = () => {
  const faker = require('faker')
  const _ = require('lodash')

  return {
    users: _.times(50, (n) => {
      return {
        id: n,
        nick: faker.internet.userName(),
        fbec_password: faker.random.alphaNumeric(
          4 + Math.floor(Math.random() * 16)
        ),
        profile_picture_url: faker.image.avatar(100, 100, true),
        fid_user: '' + faker.random.number(9999999999999999) // 16 digit
      }
    })
  }
}
