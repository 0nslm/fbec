const _ = require('lodash')
const faker = require('faker')

function randomIds(arrLen) {
  return _.times(arrLen, (m) => {
    return Math.floor(Math.random() * 50)
  })
}

function filterSame(el, i, arr) {
  if (el !== arr[i + 1]) {
    return true
  } else {
    return false
  }
}

let likeIdsDataPass

module.exports = () => {
  return {
    pages: _.times(200, (n) => {
      likeIdsDataPass = []
      likeIdsDataPass = randomIds(Math.floor(Math.random() * 50))
        .sort((a, b) => a - b)
        .filter(filterSame)
      return {
        id: n,
        liked_by_userId: likeIdsDataPass,
        name:
          faker.commerce.productAdjective() +
          ' ' +
          faker.commerce.productName(),
        fid_page: '' + faker.random.number(9999999999999999), // 16 digit
        page_picture_url: faker.image.image(200, 200, true),
        created_time: faker.date.past().toJSON()
      }
    })
  }
}
