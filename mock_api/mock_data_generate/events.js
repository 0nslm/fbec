/*
https://github.com/typicode/json-server

Relationships

To include children resources, add _embed

GET /posts?_embed=comments
GET /posts/1?_embed=comments

To include parent resource, add _expand

GET /comments?_expand=post
GET /comments/1?_expand=post

To get or create nested resources (by default one level, add custom routes for more)

GET  /posts/1/comments
POST /posts/1/comments
*/

module.exports = (relatedIdsDataPass, pageIdsCreatedDataPass) => {
  const faker = require('faker')
  const _ = require('lodash')

  let dateTemp = ''
  const rsvpsArray = [
    'attending',
    'created',
    'declined',
    'maybe',
    'unsure',
    'not_replied'
  ]

  let possibleNames = {}

  function firstUpperCase(string) {
    return string.charAt(0).toUpperCase() + string.slice(1)
  }

  function locationName() {
    possibleNames = {
      first: {
        city: faker.fake('{{name.firstName}}{{address.citySuffix}}'),
        state: faker.address.state(),
        country: faker.fake('{{address.country}}')
      },
      second: {
        bsAdj: firstUpperCase(faker.company.bsAdjective()),
        coolName: faker.fake(' {{commerce.product}} '),
        city: faker.fake('{{address.city}}')
      },
      third: {
        color: firstUpperCase(faker.commerce.color()),
        catchPhrase: faker.company.catchPhraseNoun().toUpperCase()
      }
    }

    const randomThree = Math.floor(Math.random() * 3)

    if (randomThree === 0) {
      return (
        possibleNames.first.city +
        ', ' +
        possibleNames.first.state +
        ', ' +
        possibleNames.first.country
      )
    } else if (randomThree === 1) {
      return (
        possibleNames.second.bsAdj +
        possibleNames.second.coolName +
        possibleNames.second.city
      )
    } else {
      return possibleNames.third.color + ' ' + possibleNames.third.catchPhrase
    }
  }

  function randomIds(arrLen, numMax) {
    return _.times(arrLen, (m) => {
      return Math.floor(Math.random() * numMax)
    })
  }

  function filterSame(el, i, arr) {
    if (el !== arr[i + 1]) {
      return true
    } else {
      return false
    }
  }

  return {
    events: _.times(500, (n) => {
      relatedIdsDataPass = []
      pageIdsCreatedDataPass = []
      relatedIdsDataPass = randomIds(Math.floor(Math.random() * 50), 50)
        .sort((a, b) => a - b)
        .filter(filterSame)
      pageIdsCreatedDataPass = randomIds(1 + Math.floor(Math.random() * 3), 200)
        .sort((a, b) => a - b)
        .filter(filterSame)
      return {
        id: n,
        related_to_userId: relatedIdsDataPass,
        created_by_pageId: pageIdsCreatedDataPass,
        description: faker.lorem.paragraphs(1 + Math.floor(Math.random() * 10)),
        end_time: () => {
          dateTemp = faker.date.future()
          return dateTemp.toJSON()
        },
        name: (
          faker.company.bsAdjective() +
          ' ' +
          faker.company.bsBuzz() +
          ' ' +
          faker.company.catchPhraseNoun() +
          ' ' +
          faker.random.number(30)
        ).toUpperCase(),
        place: {
          name: locationName(),
          location: {
            city: () => {
              if (possibleNames.first.city || possibleNames.second.city) {
                return possibleNames.first.city
                  ? possibleNames.first.city
                  : possibleNames.second.city
              } else {
                return faker.fake('{{name.firstName}}{{address.citySuffix}}')
              }
            },
            country: () => {
              if (possibleNames.first.country) {
                return possibleNames.first.country
              } else {
                return faker.fake('{{address.country}}')
              }
            },
            latitude: faker.address.latitude(),
            longitude: faker.address.longitude(),
            street:
              faker.address.streetName() + ' ' + faker.random.number(1000),
            zip: faker.address.zipCode()
          },
          fid_place: '' + faker.random.number(999999999999999)
        },
        start_time: () => {
          dateTemp =
            dateTemp - 1000 * 60 * 60 * (1 + Math.floor(Math.random() * 48))
          return new Date(dateTemp).toJSON()
        },
        fid_event: '' + faker.random.number(999999999999999),
        event_picture_url: faker.image.image(720, 405, true),
        rsvp_status: faker.helpers.shuffle(rsvpsArray).pop()
      }
    })
  }
}
