const fs = require('fs')

let users = require('./mock_data_generate/moc_data_manual_samples/00-01.users.onslm-boslm')().users
let pages = require('./mock_data_generate/moc_data_manual_samples/00-17.pages.00-01.users')().pages
let events = require('./mock_data_generate/moc_data_manual_samples/00-04.events.00-01.users')().events

const db = {
  users,
  pages,
  events
}

try {
  fs.writeFileSync('mock_api/db.json', JSON.stringify(db))
} catch (err) {
  console.error(err)
}
