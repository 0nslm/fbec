const fs = require('fs')
const jsonfile = require('jsonfile')

// obj with random data
let users = require('./mock_data_generate/users')().users
let pages = require('./mock_data_generate/pages')().pages
let events = require('./mock_data_generate/events')().events

const objRandom = {
  users,
  pages,
  events
}

// original obj with manual data
const file = './mock_api/db.json'

const objManual = jsonfile.readFileSync(file)

// merge both objects
users = Object.assign(objRandom.users, objManual.users)
pages = Object.assign(objRandom.pages, objManual.pages)
events = Object.assign(objRandom.events, objManual.events)

const objFinal = {
  users,
  pages,
  events
}

// write merged object into a file
try {
  fs.writeFileSync('./mock_api/db_final.json', JSON.stringify(objFinal))
} catch (err) {
  console.error(err)
}
